﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestExtJSApp.Model;
using TestExtJSApp.Model.Item;

namespace ExtJsAppTests
{
    [TestClass]
    public class CustomCollectionTest
    {
        [TestMethod]
        public void AddFirmDontThrowException()
        {
            CustomCollection<Firm> col = new CustomCollection<Firm>();
            Firm MetalGroup = new Firm(2, "МеталлГрупп", "96");

            bool result;
            try 
            { 
                col.Add(MetalGroup); 
                result = true; 
            }
            catch { result = false; }

            Assert.AreEqual(result, true);

        }

        [TestMethod]
        public void EnumeratorDontThrowException()
        {
            CustomCollection<Firm> col = new CustomCollection<Firm>();
            Firm MetalGroup = new Firm(2, "МеталлГрупп", "96");
            Firm AgroProm = new Firm(1, "АгроПром", "45");

            col.Add(MetalGroup);
            col.Add(AgroProm);

            bool result;
            try 
            { 
                foreach(Firm firm in col)
                {
                    string name = firm.Name;
                }
                result = true; 
            }
            catch
            {
                result = false;
            }

            Assert.AreEqual(result, true);
        }

        [TestMethod]
        public void EnumeratorCountReturns2()
        {
            CustomCollection<Firm> col = new CustomCollection<Firm>();
            Firm MetalGroup = new Firm(2, "МеталлГрупп", "96");
            Firm AgroProm = new Firm(1, "АгроПром", "45");

            col.Add(MetalGroup);
            col.Add(AgroProm);

            int count = 0;
            foreach (Firm firm in col)
            {
                count++;
            }

            Assert.AreEqual(count, 2);


        }

        [TestMethod]
        public void TryAdd5Addresses()
        {
            CustomCollection<Address> col = new CustomCollection<Address>();

            bool result;
            try
            {
                for (int i=0; i<=5;i++)
                {
                    Address addr = new Address(i);
                    col.Add(addr);
                }
                result = true; 
            }
            catch { result = false; }           

            Assert.AreEqual(result, true);

        }

        
    }
}

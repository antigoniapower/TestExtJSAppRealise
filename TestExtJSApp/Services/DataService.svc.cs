﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Newtonsoft.Json;
using System.ServiceModel.Activation;
using System.Collections;
using TestExtJSApp.Model;
using TestExtJSApp.Model.Item;


namespace TestExtJSApp.Services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DataService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DataService.svc or DataService.svc.cs at the Solution Explorer and start debugging.
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class DataService
    {
        /// <summary>
        /// получить данные для store
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="page"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        [OperationContract]
        public System.IO.Stream GetData(int limit, int page, int start)
        {
           // List<object> data = new List<object>() { new { id = 1, Name = "АгроПром", Value = "45" }, new { id = 2, Name = "МеталлГрупп", Value = "96" } };
            CustomCollection<Address> data = new CustomCollection<Address>();
            for (int i = 1; i <= 100;i++ )
                data.Add(new Address(i));

            return GetResponseData(data); 
        }

        private System.IO.Stream GetResponseData<T>(IEnumerable<T> list)
        {
            byte[] resultBytes = Encoding.UTF8.GetBytes(SerializeToExtJson(list));
            WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
            return new MemoryStream(resultBytes);
        }
        private string SerializeToExtJson<T>(IEnumerable<T> list)
        {
            if (list != null && list.Count() > 0)
            {
                return string.Format("{{total:{1},'data':{0}}}", JsonConvert.SerializeObject(list), list.Count());
            }
            return "{total:0,'data':0}";
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestExtJSApp.Model.Item;

namespace TestExtJSApp.Model
{
    //нельзя использовать ICollection<T>, однако функции DataService.svc принимают IEnumerable<T>
    public class CustomCollection<T> : ArrayList, IEnumerable<T> where T : Record
    {
        //реализация IEnumerable<T> GetEnumerator()
        public new IEnumerator<T> GetEnumerator()
        {
            return Cast<T>(base.GetEnumerator());//base.GetEnumerator() - ArrayList.GetEnumerator()
        }
        //преобразование IEnumerator -> IEnumerator<T>
        IEnumerator<T> Cast<T>(IEnumerator iterator)
        {
            while (iterator.MoveNext())
            {
                yield return (T)iterator.Current;
            }
        }

    }

    
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestExtJSApp.Model.Item
{
    //класс для Фирмы вида Name = "АгроПром", Value = "45"
    public class Firm:Record
    {        
        private string name;
        private string value;
        public string Name { get { return name; } }
        public string Value { get { return value; } }

        public Firm(int iD, string name, string value)
            : base(iD)
        {            
            this.name = name;
            this.value = value;
        }       
    }

    
}
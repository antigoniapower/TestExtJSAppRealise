﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestExtJSApp.Model.Item
{
    //общий родитель для Address и Firm, имеющий поле id
    //id используется в качестве ключевого для Ext.data.JsonStore
    public class Record
    {
        private int iD;
        public int id { get { return iD; } }
        public Record(int iD)
        {
            this.iD = iD;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestExtJSApp.Model.Item
{
    //Класс для адреса вида "Страна1, Город1, Дом1"
    public class Address : Record
    {
        const string countryPrefix = "Страна";
        const string cityPrefix = "Город";
        const string housePrefix = "Дом";

        private string country;
        private string city;
        private string house;

        public string Country { get { return country; } }
        public string City { get { return city; } }
        public string House { get { return house; } }

        //конструктор, создающий адрес вида "СтранаN, ГородN, ДомN"
        public Address(int iD)
            : base(iD)
        {
            this.country = countryPrefix + id;
            this.city = cityPrefix + id;
            this.house = housePrefix + id;
        }

        //конструктор для обычного адреса
        public Address(int iD, string country, string city, string house)
            : base(iD)
        {
            this.country = country;
            this.city = city;
            this.house = house;
        }

    }
}